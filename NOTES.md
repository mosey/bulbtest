Candidate Name: Ian Moss

Tasks: 
Completed tasks:
1. Improve the look and feel of the application
3. Change how monthly usage is calculated.

Time:
Task 1 - 2.5 hours
Task 2 - 1.5 hours

Notes:
The tasks took a bit longer as i wanted to ensure I understood what was expected, debugging issues related to the the testing and working through some of Typescript's errors it was showing me.
I have completed this using basic Jsx / Javascript. I don't have experience with Typescript so I stuck with what I know
I have written basic tests to determine that the component renders properly but these could be more thorough.
I kept styling to a bare minimum. As was determined by the time limit suggests by the task. I could easily add more flare and animations to it.
The totals that are being displayed on the graph for the usage estimates are being displayed for the months end per period with the first month being ignored as we don't have any comparable data to determine estimated for that period