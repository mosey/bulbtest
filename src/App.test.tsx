import * as React from 'react';
import App from './App';
import { create } from "react-test-renderer"

describe('App', () => {
  let wrapper;
  beforeEach(() => wrapper = create(<App />).toJSON())
  
  it('should render as expected', () => {
    expect(wrapper).toMatchSnapshot();
  });
  
});
