import * as React from 'react';
import { Fragment, useState, useEffect } from 'react';
import EnergyBarChart from './components/EnergyBarChart'
import EnergyMeterReadings from './components/EnergyMeterReadings'
import {GlobalStyle} from './styles/global'
import { Layout } from './styles/layout'
import { getMeterReadings} from './apiCalls/energy'

export default () => {
  const [meterReadings, setReadings] = useState([]);
  
  useEffect(() => {
    getMeterReadings().then(resp => setReadings(resp.data.electricity))
  }, []);


  return (
    <Fragment>
      <GlobalStyle />
      <Layout>
        <EnergyBarChart 
          electricity={meterReadings}
        />
        
        <EnergyMeterReadings
          electricity={meterReadings}
        />
      </Layout>
    </Fragment>
  );
};
