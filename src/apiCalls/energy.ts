import axios from 'axios'

/**
 * Fetches the list of GCPs
 *
 * @return {Promise}
 */

export async function getMeterReadings() {
    return await axios({
        method: 'get',
        url: 'https://storage.googleapis.com/bulb-interview/meterReadingsReal.json'
    });
}
