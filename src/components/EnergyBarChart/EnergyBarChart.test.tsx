import * as React from 'react';
import EnergyBarChart from './EnergyBarChart'
import { create } from "react-test-renderer"
import * as meterReadingsData from '../../data/meterReadingsSample.json';

describe('EnergyBarChart', () => {
    it('renders a loading div if the array is empty', () => {
        const bc = create(<EnergyBarChart electricity={[]} />);
        expect(bc.toJSON()).toMatchSnapshot();
    });
    
    it('renders a bar chart when supplied with information', () => {
        const bc = create(<EnergyBarChart electricity={meterReadingsData.electricity} />);
        expect(bc.toJSON()).toMatchSnapshot();
    });
});
