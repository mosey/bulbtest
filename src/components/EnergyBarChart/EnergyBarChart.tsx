// import React from 'react';
import * as React from 'react';
import * as moment from 'moment'
import { calculateAverageReadings } from '../../helpers/time'
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';
import { EnergyBarChartWrapper } from './EnergyBarChartStyles'
import { H2 } from '../../styles/layout';

export default ({ electricity = [] }) => {
    // Keeping this here for refrence
    const energyUsageData = electricity.reduce((accum, el, i, src) => {
        if(i === 0) return accum;
        accum.push({
            date: moment(el.readingDate).format('LL'),
            energyUsage: el.cumulative - src[i - 1].cumulative
        })
        return accum;
    }, []);

    const averageEneregyUsage = calculateAverageReadings(electricity);

    if(electricity.length === 0) return <div>Loading...</div>;

    return (
        <EnergyBarChartWrapper>
            <H2>Energy Usage</H2>
            <BarChart width={960} height={400} data={averageEneregyUsage}>
                <XAxis dataKey="date" />
                <YAxis dataKey="energyUsage" />
                <CartesianGrid horizontal={false} />
                <Tooltip />
                <Bar dataKey="energyUsage" fill="#03ad54" isAnimationActive={false} />
            </BarChart>
        </EnergyBarChartWrapper>
    );
};
