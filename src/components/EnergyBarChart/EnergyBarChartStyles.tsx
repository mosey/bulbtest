import styled from 'styled-components';

export const EnergyBarChartWrapper = styled.div`
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #989898;
`