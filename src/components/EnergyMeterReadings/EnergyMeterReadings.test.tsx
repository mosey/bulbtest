import * as React from 'react';
import EnergyMeterReadings from './EnergyMeterReadings'
import { create } from "react-test-renderer"
import * as meterReadingsData from '../../data/meterReadingsSample.json';

describe('EnergyMeterReadings', () => {
    it('renders a loading div if the array is empty', () => {
        const bc = create(<EnergyMeterReadings electricity={[]} />);
        expect(bc.toJSON()).toMatchSnapshot();
    });
    
    it('renders a bar chart when supplied with information', () => {
        const bc = create(<EnergyMeterReadings electricity={meterReadingsData.electricity} />);
        expect(bc.toJSON()).toMatchSnapshot();
    });
});
