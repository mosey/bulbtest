import * as React from 'react';
import * as moment from 'moment';
import { Table } from './EnergyMeterReadingStyles'
import { H2 } from '../../styles/layout';

export default ({ electricity = [] }) => {
    const meterReadings = electricity;

    const meterReadingsRows = meterReadings.map(reading => (
        <tr key={reading.readingDate}>
            <td>{moment(reading.readingDate).format("MMM Do YYYY")}</td>
            <td>{reading.cumulative}</td>
            <td>{reading.unit}</td>
        </tr>
    ));
    
    if(electricity.length === 0) return <div>Loading...</div>

    return (
        <React.Fragment>
            <H2>Meter Readings</H2>
            <Table>
                <tbody>
                    <tr>
                        <th>Date</th>
                        <th>Reading</th>
                        <th>Unit</th>
                    </tr>
                    {meterReadingsRows}
                </tbody>
            </Table>
        </React.Fragment>
    );
};
