import {calculateAverageReadings} from './time'

describe('CalculateAverageReadings', () => {
  it('Will ignore the first reading', () => {
    const singleReading = [
      { cumulative: 17580, readingDate: "2017-03-28T00:00:00.000Z", unit: "kWh" },
    ];

    expect(calculateAverageReadings(singleReading)).toEqual([]);
  });
  
  it('Tests the output of two readings and returns one value', () => {
    const readings = [
      { cumulative: 17580, readingDate: "2017-03-28T00:00:00.000Z", unit: "kWh" },
      { cumulative: 17759, readingDate: "2017-04-15T00:00:00.000Z", unit: "kWh" }
    ];
    
    expect(calculateAverageReadings(readings)).toEqual([{ date: "April 30, 2017", energyUsage: 328 }]);
  });
});
