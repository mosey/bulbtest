import * as moment from 'moment';

const _getDiffInDays = (mmt1, mmt2) => { 
    return moment(mmt1).diff(moment(mmt2), 'days'); 
}

const _isEndOfMonth = (mmt) => { 
    // startOf allows to ignore the time component 
    // we call moment(mmt) because startOf and endOf mutate the momentj object. 
    return moment.utc(mmt)
                .startOf('day')
                .isSame( 
                    moment.utc(mmt)
                    .endOf('month')
                    .startOf('day')
                ); 
}

const _getDaysUntilMonthEnd = (mmt) => { 
    return _getDiffInDays(moment.utc(mmt).endOf('month'), mmt)
}; 

export const calculateAverageReadings = (readings) => (
    readings.reduce((accum, el, i) => {
        // Ignore first reading. We have nothing to comapre it to
        if (i === 0) return accum;
        // Store values of current & previous state
        const {cumulative, readingDate} = el;
        const pre = readings[i - 1];

        // Get the difference in days of readings
        const dayDiff = _getDiffInDays(readingDate, pre.readingDate);
        // Get the total usage in that period
        const usedKwh = cumulative - pre.cumulative;
        // Get average daily usage based on total for period        
        const avDailyUsage = usedKwh / dayDiff;
        // Get remaning days
        const remainingDays = _getDaysUntilMonthEnd(readingDate);
        // Get total number of days for billing period
        const totalDays = dayDiff + remainingDays;
        // Return an object of: End of month date, estimated usage
        const estimatedUsage = totalDays * avDailyUsage;
        // Get end of month date 
        const endOfMonth = moment(readingDate).endOf('month');

        accum.push({
            date: endOfMonth.format('LL'),
            energyUsage: parseInt(estimatedUsage.toString())
        });

        return accum;
    }, [])
    
)