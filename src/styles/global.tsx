import { createGlobalStyle } from 'styled-components'
import { reset } from 'styled-reset'

export const GlobalStyle = createGlobalStyle`
  ${reset}
  
  body, html {
    font-family: sans-serif;

  }

  tr {
    border-bottom: 1px solid #989898;
  }

  th,
  td {
    padding: 10px;
  }

`