import styled from 'styled-components'
import { layoutWidth } from '../constants/styleConstants'

export const Layout = styled.section`
  width: ${layoutWidth}px;
  padding: 40px 0;
  margin: 0 auto;
  overflow: hidden;
`

export const H2 = styled.h2`
    font-size: 16px;
    margin-bottom: 20px;
    font-weight: bold;
`